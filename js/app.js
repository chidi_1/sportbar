$('document').ready(function () {
    $('[data-touch="false"]').fancybox({
        autoFocus: false,
        closeExisting: true
    });

    var current_data = new Date();

    $(".inp-datepicker").datepicker({
        dateFormat: 'dd.mm.yy',
        maxDate: current_data,
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
    });

    $('.js--select-styled').styler({
        onSelectOpened: function () {
            $('.jq-selectbox__select').removeClass('error')
        }
    });
    $('.js--file-styled').styler({
        onFormStyled: function () {
            $('.jq-file__name').html(
                '<span>Перетащие сюда фото чека. </span>' +
                '<span><i>Допускается разбивка \n' +
                'чека на 5 отдельных фото</i></span>'
            );
            $('.jq-file__browse').text('Выбрать файл')
        }
    });

    $('.js--file-styled').on('change', function () {
        $('.jq-file').removeClass('error')
    })

    $('.js--open-items').on('click', function () {
        $('.dropdown__title').removeClass('error');
        $(this).closest('.dropdown').toggleClass('opened')
    });

    $('body').on('click', function (e) {
        if (!$(e.target).parents().hasClass('dropdown')) {
            $('.dropdown').removeClass('opened')
        }
        if (!$(e.target).parents().hasClass('faq')) {
            $('.faq__answer').removeClass('visible')
        }
    });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $('.inp-phone').mask('+7(999) 999-99-99')

    // отправка формы
    $(document).on('click', '.js--form-submit', function () {

        var btn = $(this);
        var form = btn.closest('.main-form');
        var errors = false;

        $(form).find('.required').each(function () {
            var inp = $(this);
            var val = inp.prop('value');
            if (val == '') {
                inp.addClass('error');
                errors = true;
            } else {
                if (inp.hasClass('inp-mail')) {
                    if (validateEmail(val) == false) {
                        inp.addClass('error');
                        errors = true;
                    }
                }

                if (form.find('.inp-pass-double').length && form.find('.inp-pass-double').val() != form.find('.inp-pass').val()) {
                    form.find('.inp-pass-double').addClass('error');
                    form.find('.inp-pass').addClass('error');
                    form.find('.form__error').removeClass('hidden-block').find('span').text('Пароли не совпадают');
                    errors = true;
                }
            }

            if (inp.hasClass('js--select-styled') && inp.find('option:first-child').is(':selected')) {
                inp.next('.jq-selectbox__select').addClass('error');
                errors = true;
            }

            if (form.find('.dropdown__title').length && $('.dropdown__content__items input:checked').length == 0) {
                $('.dropdown__title').addClass('error');
                errors = true;
            }

            if (inp.attr('type') == 'file') {
                var parts = inp.val().split('.');
                if (parts.length == 1) {
                    $('.jq-file').addClass('error')
                    errors = true;
                }
            }
        });

        if (errors == false) {
            if (form.hasClass('form-checque')) {
                form.submit();
            } else {
                var button_value = btn.text();
                btn.text('Подождите...');

                var method = form.attr('method');
                var data = form.serialize();
                var action = form.attr('action');

                $.ajax({
                    type: method,
                    url: action,
                    data: data,
                    success: function (data) {
                        btn.text(button_value);

                        if (form.hasClass('form-refresh')) {
                            window.location = data.url;
                        } else {
                            $.fancybox.open({
                                src: '#' + form.data('success'),
                                type: 'inline',
                                touch: {
                                    vertical: 'false;'
                                }
                            });
                        }
                    },
                    error: function (data) {
                        form.find('popup__error').text(data.text);
                        btn.text('Ошибка');
                        setTimeout(function () {
                            btn.text(button_value);
                        }, 2000);
                    }
                });
            }
        }

        return false;
    });

    $('.inp').focus(function () {
        $(this).removeClass('error');
        $('.form__error').addClass('hidden-block')
    });

    $('.js--check').on('change', function () {
        var form = $(this).closest('.main-form');
        var errors = false;
        form.find('.js--check').each(function () {
            if (!$(this).is(':checked')) {
                errors = true;
            }
        });

        if (errors) {
            form.find('.btn').addClass('disabled')
        } else {
            form.find('.btn').removeClass('disabled')
        }
    });

    var date = new Date(2020, 11, 15, 23, 59, 59, 0);
    var today = new Date;

    $('.countdown').countdown(date, function (event) {
        var month = event.strftime('%m');
        var last_month = parseInt((month + ''));
        var text_month = "";

        if (last_month == 1) {
            text_month = 'месяц';
            if (last_month == 11) {
                text_month = ' месяцев'
            }
        } else {
            if (last_month == 2 || last_month == 3 || last_month == 4) {
                text_month = 'месяца';
                if (last_month == 11 || last_month == 12 || last_month == 13 || last_month == 14) {
                    text_month = ' месяцев'
                }
            } else {
                text_month = 'месяцев'
            }
        }


        var day = event.strftime('%n');
        var last_day = day;

        if (last_day > 15) {
            last_day = parseInt((day + '').substr(-1));
        }

        var text_day = "";
        if (last_day == 1) {
            text_day = 'день';
            if (last_day == 11) {
                text_day = ' дней'
            }
        } else {
            if (last_day == 2 || last_day == 3 || last_day == 4) {
                text_day = 'дня';
                if (last_day == 11 || last_day == 12 || last_day == 13 || last_day == 14) {
                    text_day = ' дней'
                }
            } else {
                text_day = 'дней'
            }
        }


        var hour = event.strftime('%H') - 1;
        var text_hour = "";
        var last_hour = hour;

        if (last_hour > 15) {
            last_hour = parseInt((hour + '').substr(-1));
        }
        if (last_hour == 1) {
            text_hour = 'час';
            if (last_hour == 11) {
                text_hour = 'часов'
            }
        } else {
            if (last_hour == 2 || last_hour == 3 || last_hour == 4) {
                text_hour = 'часа';
                if (last_hour == 11 || last_hour == 12 || last_hour == 13 || last_hour == 14) {
                    text_hour = 'часов'
                }
            } else {
                text_hour = 'часов'
            }
        }

        if (hour <= 9) {
            hour = hour.toString();
            hour = '0' + hour
        }

        var minutes = event.strftime('%M') - 1;

        var last_minutes = minutes;

        if (last_hour > 15) {
            last_minutes = parseInt((minutes + '').substr(-1));
        }

        var text_minutes = "";
        if (last_minutes == 1) {
            text_minutes = 'минута';
            if (last_minutes == 11) {
                text_minutes = 'минут'
            }
        } else {
            if (last_minutes == 2 || last_minutes == 3 || last_minutes == 4) {
                text_minutes = 'минуты';
                if (last_minutes == 11 || last_minutes == 12 || last_minutes == 13 || last_minutes == 14) {
                    text_minutes = 'минут'
                }
            } else {
                text_minutes = 'минут'
            }
        }

        if (minutes <= 9) {
            minutes = minutes.toString();
            minutes = '0' + minutes
        }

        var second = event.strftime('%S');
        var last_second = parseInt((second + '').substr(-1));
        var text_second = "";
        if (last_second == 1) {
            text_second = 'секунда';
            if (last_second == 11) {
                text_second = 'секунд'
            }
        } else {
            if (last_second == 2 || last_second == 3 || last_second == 4) {
                text_second = 'секунды';
                if (last_second == 11 || last_second == 12 || last_second == 13 || last_second == 14) {
                    text_second = 'секунд'
                }
            } else {
                text_second = 'секунд'
            }
        }

        $(this).html('<div class="countdown--el">' +
            '<div class="countdown--el-number">' + month + '</div>' +
            '<div class="countdown--el-text">' + text_month + '</div>' +
            '</div>' +
            '<div class="countdown--el">' +
            '<div class="countdown--el-number">' + day + '</div>' +
            '<div class="countdown--el-text">' + text_day + '</div>' +
            '</div>' +
            '<div class="countdown--el">' +
            '<div class="countdown--el-number">' + hour + '</div>' +
            '<div class="countdown--el-text">' + text_hour + '</div>' +
            '</div>' +
            '<div class="countdown--el">' +
            '<div class="countdown--el-number">' + minutes + '</div>' +
            '<div class="countdown--el-text">' + text_minutes + '</div>' +
            '</div>' +
            '<div class="countdown--el">' +
            '<div class="countdown--el-number">' + second + '</div>' +
            '<div class="countdown--el-text">' + text_second + '</div>' +
            '</div>'
        );
    });

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        focusOnSelect: true
    });

    $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
        $(this).addClass('active').siblings().removeClass('active')
            .closest('.tabs').find('.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });

    $('ul.tabs-product__caption').on('click', 'li:not(.active)', function () {
        $(this).addClass('active').siblings().removeClass('active')
            .closest('.tabs-product').find('.tabs-product__content').removeClass('active').eq($(this).index()).addClass('active');
    });

    $('.faq__question').on('click', function () {
        if (!device.mobile()) {
            $('.faq__wrap').addClass('open');
            $('.faq__question').removeClass('active');
            $(this).addClass('active');
        } else {
            if (!$(this).hasClass('active')) {
                $('.faq__question').removeClass('active');
                $('.faq__answer').slideUp(200)
                $(this).addClass('active').next().slideDown(200);
            }
        }
    });
    $('.js--close-faq').on('click', function () {
        $('.faq__wrap').removeClass('open');
        $('.faq__question.active').removeClass('active');
        return false;
    });

    $('.js--show-winners').on('click', function () {
        $('.winners__row').removeClass('hidden-block');

        return false;
    })

    $('.js--page-scroll').on('click', function () {
        $('.header').removeClass('open');
        $('body').removeClass('fixed');
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top + "px"
        }, {
            duration: 500
        });


        return false;
    });

    $(".scroll-wrap").mCustomScrollbar({
        scrollButtons: {enable: true},
        theme: "light-thick",
        scrollbarPosition: "outside"
    });

    $('.js--toggle-menu').on('click', function () {
        $('.header').toggleClass('open');
        $('body').toggleClass('fixed');
        return false;
    })
});